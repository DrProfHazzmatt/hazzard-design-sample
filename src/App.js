import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Header from './components/header/header';
import Content from './components/content/content';

const navItems = ['Item 1', 'Item 2', 'Item 3'];
const defaultTab = 'Item 1';
const contentOptions = [{
  bar: '#c02d2d',
  title: 'Sed fermentum tortor vitae justo rhoncus, vitae ultricies ante ultricies.',
  content: ['Fusce ultricies sit amet diam sit amet pulvinar.  Vivamus id augue neque.', 'Praesent massa magna, tempor vel blandit id, volutpat eu orci.  Pellentesque varius eros sed tempus pre.']
},
{
  bar: '#41713a',
  title: 'Sed fermentum tortor vitae justo rhoncus, vitae ultricies ante ultricies.',
  content: ['Fusce ultricies sit amet diam sit amet pulvinar.  Vivamus id augue neque.', 'Praesent massa magna, tempor vel blandit id, volutpat eu orci.  Pellentesque varius eros sed tempus pre.']
},
{
  bar: '#4d5c80',
  title: 'Sed fermentum tortor vitae justo rhoncus, vitae ultricies ante ultricies.',
  content: ['Fusce ultricies sit amet diam sit amet pulvinar.  Vivamus id augue neque.', 'Praesent massa magna, tempor vel blandit id, volutpat eu orci.  Pellentesque varius eros sed tempus pre.']
}]

class App extends Component 
{
  constructor (props) 
  {
    super(props);

    this.state = {
      navItems: navItems,
      activeTab: defaultTab,
      activeContent: contentOptions[0]
    }
  }

  changeTab = (evt, newTab = defaultTab, i) => {
    this.setState({
      activeTab: newTab,
      activeContent: contentOptions[i]
    })
  }

  render () 
  {
    return (
      <div className='container'>
        <Header navItems={this.state.navItems} activeTab={this.state.activeTab} changeTab={this.changeTab}/>
        <Content content={this.state.activeContent} />
      </div>
    )
  }
}

export default App;
