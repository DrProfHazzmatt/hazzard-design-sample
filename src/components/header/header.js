import React, { Component } from 'react';

import Navigation from '../navigation/navigation';

import './header.css';

const header = 'CRAS LACINIA ANTE TORTOR?';

class Header extends Component
{
    constructor (props) 
    {
        super(props);
    }

    render () 
    {
        return (
            <div className='headerContainer'>
                <h1>{header}</h1>
                <Navigation navItems={this.props.navItems} activeTab={this.props.activeTab} changeTab={this.props.changeTab} />
            </div>
        )
    }
}

export default Header;