import React, { Component } from 'react';
import { CSSTransitionGroup } from 'react-transition-group';

import './content.css';

class Content extends Component
{
    constructor (props)
    {
        super(props);
    }

    componentWillUnmount () {
        console.log('component unmounting');
    }

    render () {
        return (
            
                <div className='contentContainer'>
                    <CSSTransitionGroup
                        transitionName='fadeIn'
                        transitionEnterTimeout={500}
                        transitionLeave={false}>
                        <div key={this.props.content.bar} className='colorBar' style={{backgroundColor: this.props.content.bar}} />

                        <h1 key={'title' + this.props.content.bar} >{this.props.content.title}</h1>

                        {
                            this.props.content.content.map((line, i) => {
                                return(
                                    <p key={i + this.props.content.bar}>{line}</p>
                                );
                            })
                        }
                    </CSSTransitionGroup>
                </div>
        )
    }
}

export default Content;