import _ from 'lodash';
import React, { Component } from 'react';

import './navigation.css';

class Navigation extends Component 
{
    constructor (props) 
    {
        super(props);
    }

    render () 
    {
        return (
            <div className='navRow'>
                {
                    this.props.navItems.map((item, i) => {
                        return (
                            <div key={item} className='navBtn' style={this.props.activeTab === item ? {backgroundColor: 'white'} : {}} onClick={(evt) => this.props.changeTab(evt, item, i)}>
                                {item}
                            </div>
                        )
                    })
                }
            </div>
        );
    }
}

export default Navigation;